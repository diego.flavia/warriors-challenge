package br.com.treasy;

import br.com.treasy.models.Node;
import br.com.treasy.repositories.NodeRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class NodeRepositoryUtils {

    @Autowired
    private NodeRepository repo;

    @Getter
    private Set<Node> nodes = new HashSet<>();

    public Node saveNode(String prefix) {
        return saveNode(prefix, null, true);
    }

    public Node saveNode(String prefix, boolean addToList) {
        return saveNode(prefix, null, addToList);
    }

    public Node saveNode(String prefix, Node parent) {
        return saveNode(prefix, parent, true);
    }

    public Node saveNode(String prefix, Node parent, boolean addToList) {
        Node node = new Node();
        node.setCode(prefix + "_code");
        node.setDescription(prefix + "_description");
        node.setDetail(prefix + "_detail");
        node.setParent(parent);
        repo.save(node);
        if (addToList) {
            nodes.add(node);
        }
        return node;
    }

}
