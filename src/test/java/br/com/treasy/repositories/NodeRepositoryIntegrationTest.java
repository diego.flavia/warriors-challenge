package br.com.treasy.repositories;

import br.com.treasy.NodeRepositoryUtils;
import br.com.treasy.models.Node;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class NodeRepositoryIntegrationTest {

    @Autowired
    private NodeRepository repo;

    @Autowired
    private NodeRepositoryUtils repoUtils;

    @Test
    public void whenSave() {
        //given
        Node oneNode = new Node();
        oneNode.setCode("one_code");
        oneNode.setDescription("one_description");
        oneNode.setDetail("one_detail");

        //when
        Node nodeSaved = repo.save(oneNode);

        //then
        assertThat(nodeSaved).isEqualTo(oneNode);
    }

    @Test
    public void whenFindById_thenReturnNode() {
        //given
        Node oneNode = new Node();
        oneNode.setCode("one_code");
        oneNode.setDescription("one_description");
        oneNode.setDetail("one_detail");
        repo.save(oneNode);

        Node anotherNode = new Node();
        anotherNode.setCode("another_code");
        anotherNode.setDescription("another_description");
        anotherNode.setDetail("another_detail");
        repo.save(anotherNode);

        //when
        Node nodeFound = repo.findById(oneNode.getId()).orElse(null);

        //then
        assertThat(nodeFound).isEqualTo(oneNode);
    }

    @Test
    public void whenFindRoot_thenReturnRoot() {
        //given
        Node rootNode = repoUtils.saveNode("root");
        Node oneNode = repoUtils.saveNode("one", rootNode);
        Node secondNode = repoUtils.saveNode("second", rootNode);
        Node anotherNode = repoUtils.saveNode("another", oneNode);

        //when
        Node nodeFound = repo.findRoot().orElse(null);

        //then
        assertThat(nodeFound).isEqualTo(rootNode);
    }

    @Transactional(readOnly = true)
    @Test
    public void whenStreamAll() {
        //given
        Node rootNode = repoUtils.saveNode("root");
        Node oneNode = repoUtils.saveNode("one", rootNode);
        repoUtils.saveNode("second", rootNode);
        repoUtils.saveNode("another", oneNode);
        Set<Node> nodes = repoUtils.getNodes();

        //when
        repo.streamAll().forEach(node -> {
            //then
            assertThat(nodes.contains(node)).isTrue();
        });

    }

    @Transactional(readOnly = true)
    @Test
    public void whenStreamByParent() {
        //given
        Node rootNode = repoUtils.saveNode("root");
        Node oneNode = repoUtils.saveNode("one", rootNode);
        Node secondNode = repoUtils.saveNode("second", rootNode);
        Node firstChildNode = repoUtils.saveNode("oneChild", oneNode);
        Node secondChildNode = repoUtils.saveNode("secondChild", oneNode);
        repoUtils.saveNode("noChild", secondNode);
        //expected childs
        Set<Node> nodes = new HashSet<>(2);
        nodes.add(firstChildNode);
        nodes.add(secondChildNode);

        //when
        repo.streamByParentId(oneNode.getId()).forEach(node -> {
            //then
            assertThat(nodes.contains(node)).isTrue();
        });

    }

}
