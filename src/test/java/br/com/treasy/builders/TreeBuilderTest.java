package br.com.treasy.builders;

import br.com.treasy.NodeRepositoryUtils;
import br.com.treasy.dtos.TreeNodeDTO;
import br.com.treasy.models.Node;
import br.com.treasy.repositories.NodeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TreeBuilderTest {

    @Autowired
    private NodeRepository repo;

    @Autowired
    private NodeRepositoryUtils repoUtils;

    @Transactional(readOnly = true)
    @Test
    public void streamAll() {
        //given
        Node rootNode = repoUtils.saveNode("root");
        /**/Node node_1 = repoUtils.saveNode("node_1", rootNode);
        /**/repoUtils.saveNode("node_1_1", node_1);
        /**/Node node_1_2 = repoUtils.saveNode("node_1_2", node_1);
        /**//**/repoUtils.saveNode("node_1_2_1", node_1_2);
        /**/Node node_1_3 = repoUtils.saveNode("node_1_3", node_1);
        /**//**/repoUtils.saveNode("node_1_3_1", node_1_3);
        /**//**/repoUtils.saveNode("node_1_3_2", node_1_3);
        Node node_2 = repoUtils.saveNode("node_2", rootNode);
        /**/repoUtils.saveNode("node_2_1", node_2);
        Node node_3 = repoUtils.saveNode("node_3", rootNode);
        /**/Node node_3_1 = repoUtils.saveNode("node_3_1", node_3);
        /**//**/Node node_3_1_1 = repoUtils.saveNode("node_3_1_1", node_3_1);
        /**//**//**/repoUtils.saveNode("node_3_1_1_1", node_3_1_1);
        /**//**//**/repoUtils.saveNode("node_3_1_1_2", node_3_1_1);

        //when
        TreeNodeDTO treeFromDB = TreeBuilder.novo(repo.streamAll()).build();
        TreeNodeDTO treeFromTest = TreeBuilder.novo(repoUtils.getNodes().stream()).build();

        //then
        recursiveCheck(treeFromDB, treeFromTest);
    }

    private void recursiveCheck(TreeNodeDTO dbDTO, TreeNodeDTO testDTO) {
        checkTreeNodeDTO(dbDTO, testDTO);
        //Assert Size
        final Set<TreeNodeDTO> testChildren = testDTO.getChildren();
        final Set<TreeNodeDTO> dbChildren = testDTO.getChildren();
        assertThat(dbChildren.size()).isEqualTo(testChildren.size());
        //Assert Children
        final Iterator<TreeNodeDTO> testIterator = testChildren.iterator();
        dbChildren.forEach(childDbDTO -> recursiveCheck(childDbDTO, testIterator.next()));
    }

    private void checkTreeNodeDTO(TreeNodeDTO dbDTO, TreeNodeDTO testDTO) {
        assertThat(dbDTO.getId()).isEqualTo(testDTO.getId());
        assertThat(dbDTO.getCode()).isEqualTo(testDTO.getCode());
        assertThat(dbDTO.getDescription()).isEqualTo(testDTO.getDescription());
        assertThat(dbDTO.getDetail()).isEqualTo(testDTO.getDetail());
        assertThat(dbDTO.getParentId()).isEqualTo(testDTO.getParentId());
    }

}
