package br.com.treasy.rules;

import br.com.treasy.NodeRepositoryUtils;
import br.com.treasy.dtos.DefaultNodeDTO;
import br.com.treasy.dtos.SingleLevelNodeDTO;
import br.com.treasy.dtos.TreeNodeDTO;
import br.com.treasy.exceptions.BusinessException;
import br.com.treasy.models.Node;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class NodeRuleTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private NodeRule rule;

    @Autowired
    private NodeRepositoryUtils repoUtils;

    @Test
    public void whenCreateRootMissing_thenThrow() {
        //given
        DefaultNodeDTO node_1 = buildDTO("node_1", 1L);

        //then
        thrown.expect(BusinessException.class);

        //when
        rule.create(node_1);
    }

    @Test
    public void whenCreateRootMissing_thenReturnNode() {
        //given
        DefaultNodeDTO nodeRootDTO = buildDTO("node_root" );

        //when
        Node node = rule.create(nodeRootDTO);

        //then
        assertThat(node.getCode()).isEqualTo(node.getCode());
    }

    @Test
    public void whenUpdateNodeMissing_thenThrow() {
        //given
        DefaultNodeDTO nodeRootDTO = buildDTO("node_root");
        nodeRootDTO.setId(1L);

        //then
        thrown.expect(BusinessException.class);

        //when
        Node node = rule.update(nodeRootDTO);
    }

    @Test
    public void whenUpdateNode_thenReturnNode() {
        //given
        DefaultNodeDTO nodeDTO = buildDTO("node");
        Node nodeCreated = rule.create(nodeDTO);
        nodeDTO.setId(nodeCreated.getId());

        //when
        Node nodeUpdated = rule.update(nodeDTO);

        //then
        assertThat(nodeUpdated.getId()).isEqualTo(nodeCreated.getId());
    }

    @Test
    public void whenUpdateNodeNullParent_thenThrow() {
        //given
        DefaultNodeDTO rootDTO = buildDTO("root");
        Node rootCreated = rule.create(rootDTO);
        DefaultNodeDTO nodeDTO = buildDTO("node", rootCreated.getId());
        Node nodeCreated = rule.create(nodeDTO);
        nodeDTO.setId(nodeCreated.getId());

        //then
        thrown.expect(BusinessException.class);

        //when
        nodeDTO.setParentId(null);
        Node nodeUpdated = rule.update(nodeDTO);
    }

    @Transactional(readOnly = true)
    @Test
    public void whenFindByParentAbsent_thenThrow() {
        //given
        DefaultNodeDTO rootDTO = buildDTO("root");

        //then
        thrown.expect(BusinessException.class);

        //when
        Collection<SingleLevelNodeDTO> childrenDTO = rule.findByParentId(rootDTO.getId());
    }

    @Transactional(readOnly = true)
    @Test
    public void whenFindByParent_thenReturnCollection(){
        //given
        Node rootNode = repoUtils.saveNode("root", false);
        /**/Node node_1 = repoUtils.saveNode("node_1", rootNode, false);
        /**//**/Node node_1_1 = repoUtils.saveNode("node_1_1", node_1);
        /**//**/Node node_1_2 = repoUtils.saveNode("node_1_2", node_1);
        /**//**//**/repoUtils.saveNode("node_1_2_1", node_1_2, false);
        /**//**/Node node_1_3 = repoUtils.saveNode("node_1_3", node_1);
        /**//**//**/repoUtils.saveNode("node_1_3_1", node_1_3, false);
        /**//**//**/repoUtils.saveNode("node_1_3_2", node_1_3, false);
        /**//**/Node node_1_4 = repoUtils.saveNode("node_1_4", node_1);
        /**//**/Node node_1_5 = repoUtils.saveNode("node_1_5", node_1);
        /**/Node node_2 = repoUtils.saveNode("node_2", rootNode, false);
        /**//**/repoUtils.saveNode("node_2_1", node_2, false);
        /**/Node node_3 = repoUtils.saveNode("node_3", rootNode, false);
        /**//**/Node node_3_1 = repoUtils.saveNode("node_3_1", node_3, false);
        /**//**//**/Node node_3_1_1 = repoUtils.saveNode("node_3_1_1", node_3_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_1", node_3_1_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_2", node_3_1_1, false);

        //when
        Collection<SingleLevelNodeDTO> childrenDTO = rule.findByParentId(node_1.getId());
        Collection<SingleLevelNodeDTO> nodes = repoUtils.getNodes().stream().map(SingleLevelNodeDTO::new).collect(Collectors.toSet());

        //then
        assertThat(childrenDTO.size()).isEqualTo(nodes.size());
        childrenDTO.stream().forEach(child -> {
            assertThat(nodes.contains(child)).isTrue();
        });
    }

    @Transactional(readOnly = true)
    @Test
    public void whenGetRoot_thenReturnRoot(){
        //given
        Node rootNode = repoUtils.saveNode("root", false);
        /**/Node node_1 = repoUtils.saveNode("node_1", rootNode, false);
        /**//**/Node node_1_1 = repoUtils.saveNode("node_1_1", node_1);
        /**//**/Node node_1_2 = repoUtils.saveNode("node_1_2", node_1);
        /**//**//**/repoUtils.saveNode("node_1_2_1", node_1_2, false);
        /**//**/Node node_1_3 = repoUtils.saveNode("node_1_3", node_1);
        /**//**//**/repoUtils.saveNode("node_1_3_1", node_1_3, false);
        /**//**//**/repoUtils.saveNode("node_1_3_2", node_1_3, false);
        /**//**/Node node_1_4 = repoUtils.saveNode("node_1_4", node_1);
        /**//**/Node node_1_5 = repoUtils.saveNode("node_1_5", node_1);
        /**/Node node_2 = repoUtils.saveNode("node_2", rootNode, false);
        /**//**/repoUtils.saveNode("node_2_1", node_2, false);
        /**/Node node_3 = repoUtils.saveNode("node_3", rootNode, false);
        /**//**/Node node_3_1 = repoUtils.saveNode("node_3_1", node_3, false);
        /**//**//**/Node node_3_1_1 = repoUtils.saveNode("node_3_1_1", node_3_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_1", node_3_1_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_2", node_3_1_1, false);

        //when
        TreeNodeDTO rootDTO = rule.getRoot();

        //then
        assertThat(rootDTO.getCode()).isEqualTo(rootNode.getCode());
    }

    public DefaultNodeDTO buildDTO(String prefix) {
        return buildDTO(prefix, null);
    }

    public DefaultNodeDTO buildDTO(String prefix, Long parentId) {
        return DefaultNodeDTO.builder()
                .code(prefix + "_code")
                .description(prefix + "_description")
                .detail(prefix + "_detail")
                .parentId(parentId)
                .build();
    }

}
