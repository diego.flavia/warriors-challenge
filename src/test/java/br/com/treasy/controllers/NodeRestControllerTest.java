package br.com.treasy.controllers;

import br.com.treasy.NodeRepositoryUtils;
import br.com.treasy.dtos.DefaultNodeDTO;
import br.com.treasy.models.Node;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class NodeRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private NodeRepositoryUtils repoUtils;

    @Test
    public void givenNode_whenCreate_thenReturnId() throws Exception {

        DefaultNodeDTO rootDTO = DefaultNodeDTO.builder().code("node").description("node desc").detail("node detail").build();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/node")
                .content(asJsonString(rootDTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated())
                .andReturn();

        Long.valueOf(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void givenWrongJson_whenCreate_thenThrow() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/node")
                .content("{\"id\":}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    //TODO Falta verificar validações dos campos

    @Test
    public void givenNode_whenUpdate_thenReturnId() throws Exception {

        DefaultNodeDTO rootDTO = DefaultNodeDTO.builder().code("node").description("node desc").detail("node detail").build();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/node")
                .content(asJsonString(rootDTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated())
                .andReturn();

        rootDTO.setId(Long.valueOf(mvcResult.getResponse().getContentAsString()));
        rootDTO.setCode("new code");

        mvc.perform(MockMvcRequestBuilders
                .put("/node")
                .content(asJsonString(rootDTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void whenFindRoot_thenReturnTree() throws Exception {

        //given
        Node rootNode = repoUtils.saveNode("root", false);
        /**/Node node_1 = repoUtils.saveNode("node_1", rootNode, false);
        /**//**/Node node_1_1 = repoUtils.saveNode("node_1_1", node_1);
        /**//**/Node node_1_2 = repoUtils.saveNode("node_1_2", node_1);
        /**//**//**/repoUtils.saveNode("node_1_2_1", node_1_2, false);
        /**//**/Node node_1_3 = repoUtils.saveNode("node_1_3", node_1);
        /**//**//**/repoUtils.saveNode("node_1_3_1", node_1_3, false);
        /**//**//**/repoUtils.saveNode("node_1_3_2", node_1_3, false);
        /**//**/Node node_1_4 = repoUtils.saveNode("node_1_4", node_1);
        /**//**/Node node_1_5 = repoUtils.saveNode("node_1_5", node_1);
        /**/Node node_2 = repoUtils.saveNode("node_2", rootNode, false);
        /**//**/repoUtils.saveNode("node_2_1", node_2, false);
        /**/Node node_3 = repoUtils.saveNode("node_3", rootNode, false);
        /**//**/Node node_3_1 = repoUtils.saveNode("node_3_1", node_3, false);
        /**//**//**/Node node_3_1_1 = repoUtils.saveNode("node_3_1_1", node_3_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_1", node_3_1_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_2", node_3_1_1, false);

        mvc.perform(MockMvcRequestBuilders
                .get("/node")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(rootNode.getId().intValue())))
                .andExpect(jsonPath("$.code", is(rootNode.getCode())))
                .andExpect(jsonPath("$.description", is(rootNode.getDescription())))
                .andExpect(jsonPath("$.detail", is(rootNode.getDetail())))
                .andExpect(jsonPath("$.children", hasSize(3)));
    }

    @Test
    public void whenFindByParentId_thenReturnChildren() throws Exception {

        //given
        Node rootNode = repoUtils.saveNode("root", false);
        /**/Node node_1 = repoUtils.saveNode("node_1", rootNode, false);
        /**//**/Node node_1_1 = repoUtils.saveNode("node_1_1", node_1);
        /**//**/Node node_1_2 = repoUtils.saveNode("node_1_2", node_1);
        /**//**//**/repoUtils.saveNode("node_1_2_1", node_1_2, false);
        /**//**/Node node_1_3 = repoUtils.saveNode("node_1_3", node_1);
        /**//**//**/repoUtils.saveNode("node_1_3_1", node_1_3, false);
        /**//**//**/repoUtils.saveNode("node_1_3_2", node_1_3, false);
        /**//**/Node node_1_4 = repoUtils.saveNode("node_1_4", node_1);
        /**//**/Node node_1_5 = repoUtils.saveNode("node_1_5", node_1);
        /**/Node node_2 = repoUtils.saveNode("node_2", rootNode, false);
        /**//**/repoUtils.saveNode("node_2_1", node_2, false);
        /**/Node node_3 = repoUtils.saveNode("node_3", rootNode, false);
        /**//**/Node node_3_1 = repoUtils.saveNode("node_3_1", node_3, false);
        /**//**//**/Node node_3_1_1 = repoUtils.saveNode("node_3_1_1", node_3_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_1", node_3_1_1, false);
        /**//**//**//**/repoUtils.saveNode("node_3_1_1_2", node_3_1_1, false);

        mvc.perform(MockMvcRequestBuilders
                .get("/node/{parentId}", rootNode.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));

        mvc.perform(MockMvcRequestBuilders
                .get("/node/{parentId}", node_1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));

        mvc.perform(MockMvcRequestBuilders
                .get("/node/{parentId}", node_2.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));

        mvc.perform(MockMvcRequestBuilders
                .get("/node/{parentId}", node_3_1_1.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void givenWrongParentId_whenFindByParentId_thenThrow() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/node/{parentId}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
