package br.com.treasy.controllers;

import br.com.treasy.dtos.DefaultNodeDTO;
import br.com.treasy.dtos.SingleLevelNodeDTO;
import br.com.treasy.dtos.TreeNodeDTO;
import br.com.treasy.models.Node;
import br.com.treasy.rules.NodeRule;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * Controller responsável pelas operações relacionadas ao Nó {@link Node}
 */
@Api(value = "Estrutura de árvore")
@RestController
@RequestMapping(value = "/node", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class NodeRestController {

    @Autowired
    NodeRule rules;

    /**
     * Cadastra um nó
     *
     * @param dto campos do nó
     * @return Id do nó cadastrado
     */
    @ApiOperation(value = "Cadastrar Nó")
    @PostMapping
    public ResponseEntity<Long> createNode(@Valid @RequestBody DefaultNodeDTO dto) {
        Node nodeCreated = rules.create(dto);
        return new ResponseEntity(nodeCreated.getId(), HttpStatus.CREATED);
    }

    /**
     * Atualiza um nó
     *
     * @param dto campos do nó
     * @return Id do nó atualizado
     */
    @ApiOperation(value = "Atualizar Nó")
    @PutMapping
    public ResponseEntity<Long> upadteNode(@Valid @RequestBody DefaultNodeDTO dto) {
        Node nodeUpdated = rules.update(dto);
        return new ResponseEntity(nodeUpdated.getId(), HttpStatus.OK);
    }

    /**
     * Retorna a árvore completo de nós
     *
     * @return the tree
     */
    @ApiOperation(value = "Árvore completa")
    @Transactional(readOnly = true)
    @GetMapping
    public ResponseEntity<TreeNodeDTO> findRoot() {
        TreeNodeDTO rootNode = rules.getRoot();
        return new ResponseEntity(rootNode, HttpStatus.OK);
    }


    /**
     * Retorna uma lista de nós conforme pai
     *
     * @param parentId Id do nó pai
     * @return Lista de nós
     */
    @ApiOperation(value = "Nós filhos")
    @Transactional(readOnly = true)
    @GetMapping(value = "/{parentId}")
    public ResponseEntity<Collection<SingleLevelNodeDTO>> findByParentId(@PathVariable("parentId") Long parentId) {
        Collection<SingleLevelNodeDTO> nodes = rules.findByParentId(parentId);
        return new ResponseEntity(nodes, HttpStatus.OK);
    }

}
