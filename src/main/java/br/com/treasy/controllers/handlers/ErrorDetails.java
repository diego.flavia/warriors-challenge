package br.com.treasy.controllers.handlers;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe para detalhamento das mensagens de erro
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorDetails {

    @Getter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime timestamp;

    @Getter
    private List<Error> messages = new ArrayList<>(0);

    public ErrorDetails() {
        this.timestamp = LocalDateTime.now();
    }

    public ErrorDetails(String message) {
        this();
        addError(message);
    }

    public void addError(String message) {
        addError(null, message);
    }

    public void addError(String field, String message) {
        addError(new Error(field, message));
    }

    public void addError(Error error) {
        messages.add(error);
    }

    @Getter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Error {

        private String field;
        private String message;

        public Error(String field, String message) {
            this.message = message;
            this.field = field;
        }
    }
}
