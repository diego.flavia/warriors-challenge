package br.com.treasy.controllers.handlers;

import br.com.treasy.exceptions.BusinessException;
import br.com.treasy.models.Constraints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Locale;

/**
 * Handler para tratamento de exceções dos controllers rest
 */
@ControllerAdvice
@RestController
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception exc, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(exc.getMessage());
        return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BusinessException.class)
    public final ResponseEntity<ErrorDetails> handleBusinessExceptions(BusinessException bex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(messageSource.getMessage(bex.getMessage(), bex.getParams(), Locale.getDefault()));
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(
            DataIntegrityViolationException ex, WebRequest request) {
        Constraints constraint = Constraints.getFrom(ex.getMostSpecificCause().getLocalizedMessage());
        String key =  Constraints.PREFIX;
        if (constraint != null){
            key = constraint.getFullName();
        }
        ErrorDetails errorDetails = new ErrorDetails(messageSource.getMessage(key, null, Locale.getDefault()));
        return new ResponseEntity(errorDetails, HttpStatus.CONFLICT);
    }

    @Override
    public final ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException hmnrex,
                                                                                    HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(messageSource.getMessage("json.malformed", null, Locale.getDefault()));
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        ErrorDetails errorDetails = new ErrorDetails();
        for (FieldError fieldError : fieldErrors) {
            errorDetails.addError(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }
}
