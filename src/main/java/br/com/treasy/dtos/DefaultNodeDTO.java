package br.com.treasy.dtos;

import br.com.treasy.models.Node;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * DTO padrão para Nó
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DefaultNodeDTO {

    protected long id;

    @EqualsAndHashCode.Include
    @ToString.Include
    @NotEmpty
    @Size(min = 1, max = 40)
    protected String code;

    @NotEmpty
    @Size(min = 1, max = 100)
    protected String description;

    @NotEmpty
    @Size(min = 1, max = 1000)
    protected String detail;

    protected Long parentId;

    public DefaultNodeDTO(Node node) {
        this.id = node.getId();
        this.code = node.getCode();
        this.description = node.getDescription();
        this.detail = node.getDetail();
        if (node.getParent() != null) {
            this.parentId = node.getParent().getId();
        }
    }
}
