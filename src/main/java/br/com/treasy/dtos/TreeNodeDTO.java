package br.com.treasy.dtos;

import br.com.treasy.models.Node;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * DTO para listagem da árvore de Nó
 */
@JsonPropertyOrder({"id", "code", "description", "parentId", "detail", "children"})
public class TreeNodeDTO extends DefaultNodeDTO {

    @Getter
    @Builder.Default
    private Set<TreeNodeDTO> children = new HashSet<>(0);

    public TreeNodeDTO(Node node) {
        super(node);
    }

}
