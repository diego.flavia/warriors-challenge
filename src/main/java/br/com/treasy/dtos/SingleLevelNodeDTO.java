package br.com.treasy.dtos;

import br.com.treasy.models.Node;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Getter;

/**
 * DTO para listagem de Nó em um único nível
 */
@JsonPropertyOrder({"id", "code", "description", "parentId", "detail", "hasChildren"})
public class SingleLevelNodeDTO extends DefaultNodeDTO {

    @Getter
    @Builder.Default
    private boolean hasChildren;

    public SingleLevelNodeDTO(Node node) {
        super(node);
        this.hasChildren = !node.getChildren().isEmpty();
    }

}
