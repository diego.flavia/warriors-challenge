package br.com.treasy.rules;

import br.com.treasy.builders.TreeBuilder;
import br.com.treasy.dtos.DefaultNodeDTO;
import br.com.treasy.dtos.SingleLevelNodeDTO;
import br.com.treasy.dtos.TreeNodeDTO;
import br.com.treasy.exceptions.BusinessException;
import br.com.treasy.models.Node;
import br.com.treasy.repositories.NodeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Componente reponsável por aplicar regras na criação e atualização dos nós
 */
@Component
public class NodeRule {

    @Autowired
    NodeRepository repo;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Cria um nó {@link Node} de acordo com o dto {@link DefaultNodeDTO} recebido
     *
     * @param dto parâmetros do nó
     * @return Nó
     * @throws BusinessException
     */
    public Node create(DefaultNodeDTO dto) throws BusinessException {
        checkRootOrThrow(dto);
        Optional<Node> parent = getParent(dto);
        Node node = modelMapper.map(dto, Node.class);
        parent.ifPresent(node::setParent);
        return repo.save(node);
    }

    /**
     * Atualiza um nó {@link Node} de acordo com o dto {@link DefaultNodeDTO} recebido
     *
     * @param dto parâmetros do nó
     * @return Nó
     * @throws BusinessException
     */
    public Node update(DefaultNodeDTO dto) throws BusinessException {
        Node node = repo.findById(dto.getId())
                .orElseThrow(() -> new BusinessException("node.notFound"));
        if (node.getParent() != null) {
            checkRootOrThrow(dto);
        }
        Optional<Node> parent = getParent(dto);
        modelMapper.map(dto, node);
        parent.ifPresent(node::setParent);
        return repo.save(node);
    }

    /**
     * Retorna o nó raiz de toda a estrutura
     *
     * @return Nó raiz {@link TreeNodeDTO}
     */
    public TreeNodeDTO getRoot() {
        return TreeBuilder.novo(repo.streamAll()).build();
    }

    /**
     * Retorna um coleção {@link Collection} de Nós filhos, de acordo com o Id do nó pai recebido
     *
     * @param parentId Id do nó pai
     * @return Coleção de Nó
     */
    public Collection<SingleLevelNodeDTO> findByParentId(Long parentId) {
        getParent(parentId).orElseThrow(() -> new BusinessException("node.parent.notFound"));
        return repo.streamByParentId(parentId).map(SingleLevelNodeDTO::new).collect(Collectors.toSet());
    }

    /**
     * Retorna o nó pai caso encontrado, ou lança exception {@link BusinessException}
     *
     * @param dto Nó buscado
     * @return Nó encontrado
     */
    private Optional<Node> getParent(DefaultNodeDTO dto) {
        return getParent(dto.getParentId());
    }
    private Optional<Node> getParent(Long parentId) {
        if (parentId != null) {
            Optional<Node> parent = repo.findById(parentId);
            parent.orElseThrow(() -> new BusinessException("node.parent.notFound"));
            return parent;
        }
        return Optional.empty();

    }

    /**
     * Verifica se já é nó raiz ou se já existe outro nó raiz, caso já exista lança exception {@link BusinessException}
     *
     * @param dto Nó para verificar
     */
    private void checkRootOrThrow(DefaultNodeDTO dto) {
        if (dto.getParentId() == null) {
            repo.findRoot().ifPresent(root -> {
                if (!root.getId().equals(dto.getId())) {
                    throw new BusinessException("node.root.exists");
                }
            });
        }
    }
}
