package br.com.treasy.repositories;

import br.com.treasy.models.Node;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Repository responsável pelas operações em bando de dados relacionadas ao Nó {@link Node}
 */
public interface NodeRepository extends Repository<Node, Long> {

    /**
     * Salva um Nó na base de dados
     *
     * @param node Nó {@link Node} a ser salvo
     * @return Nó {@link Node} salvo
     */
    Node save(Node node);

    /**
     * Retorna um Nó de acordo com o id recebido
     *
     * @param id Id do Nó {@link Node} buscado
     * @return Nó {@link Node} encontrado, ou null
     */
    Optional<Node> findById(Long id);

    /**
     * Retorna o Nó raiz da estruturas
     *
     * @return Nó {@link Node} raiz encontrado, ou null
     */
    @Query(value = "select obj from Node obj where obj.parent is null")
    Optional<Node> findRoot();

    /**
     * Retorna um Stream de Nó {@link Node} de toda a estrutura
     *
     * @return Stream de Nó {@link Node}
     */
    @Query(value = "select obj from Node obj order by obj.parent")
    Stream<Node> streamAll();

    /**
     * Retorna um Stream de Nó {@link Node} com os filhos do Id do Nó {@link Node} pai recebido
     *
     * @param parentId Id do Nó {@link Node} pai
     * @return Stream de Nó {@link Node}
     */
    @Query(value = "select obj from Node obj " +
            "join fetch obj.parent par " +
            "left join fetch obj.children " +
            "where par.id = ?1 " +
            "order by obj.parent")
    Stream<Node> streamByParentId(Long parentId);

}
