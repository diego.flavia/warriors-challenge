package br.com.treasy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarriorsChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarriorsChallengeApplication.class, args);
	}

}
