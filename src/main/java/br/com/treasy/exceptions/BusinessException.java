package br.com.treasy.exceptions;

import lombok.Getter;

/**
 * Exception padrão para exceções de regras
 */
public class BusinessException extends RuntimeException {

    @Getter
    String[] params;

    public BusinessException(String message, String... params) {
        super(message);
        this.params = params;
    }
}
