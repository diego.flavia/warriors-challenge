package br.com.treasy.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entidade responsável pela estrutura de um Nó
 *
 * {@link Constraints}
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "node",
        uniqueConstraints = {
                @UniqueConstraint(name = "uk_node_code", columnNames = {"code"})
        },
        indexes = {
                @Index(name = "idx_node_parent", columnList = "parentId")
        }
)
public class Node {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @EqualsAndHashCode.Include
    @ToString.Include
    @Column(length = 40, nullable = false)
    private String code;

    @Column(length = 100, nullable = false)
    private String description;

    @Column(length = 1000, nullable = false)
    private String detail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId", foreignKey = @ForeignKey(name = "fk_node_parent"))
    private Node parent;

    @JsonIgnore
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<Node> children = new ArrayList<>(0);

}
