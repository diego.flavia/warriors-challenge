package br.com.treasy.models;

import lombok.Getter;

import java.util.Arrays;

/**
 * Lista de Constraints para mensagens de validação de integridade
 */
public enum Constraints {

    UK_NODE_CODE("uk_node_code");

    public static final String PREFIX = "constraint.violation";

    @Getter
    private String name;

    Constraints(String name) {
        this.name = name;
    }

    public String getFullName(){
        return PREFIX + "." + getName();
    }

    public static Constraints getFrom(String message){
        return Arrays.stream(Constraints.values())
                .filter(constraint -> message.contains(constraint.getName()))
                .findFirst().orElse(null);
    }

}
