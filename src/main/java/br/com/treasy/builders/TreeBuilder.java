package br.com.treasy.builders;

import br.com.treasy.dtos.TreeNodeDTO;
import br.com.treasy.models.Node;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Builder reponsável por criar uma árvore com a estrutura de Nó {@link TreeNodeDTO}
 */
public class TreeBuilder {

    private TreeNodeDTO root;
    private Map<Long, TreeNodeDTO> nodeMap = new HashMap<>();

    /**
     * Cria uma nova instância do builder, conforme Stream de Nó {@link Node}
     *
     * @param stream Stream de Nó {@link Node}
     * @return Builder da árvore
     */
    public static TreeBuilder novo(Stream<Node> stream) {
        TreeBuilder builder = new TreeBuilder();
        return builder.addNode(stream);
    }

    /**
     * Retorna o Nó raiz da árvore
     *
     * @return Nó raiz {@link TreeNodeDTO}
     */
    public TreeNodeDTO build() {
        return root;
    }

    /**
     * Adiciona um Nó {@link TreeNodeDTO} na árvore, conforme Straem de Nó {@link Node}
     *
     * @param stream Stream de Nó {@link Node}
     * @return Builder da árvore
     */
    private TreeBuilder addNode(Stream<Node> stream) {
        stream.forEach(this::addNode);
        return this;
    }

    /**
     * Adiciona um Nó {@link TreeNodeDTO} na árvore, conforme Nó {@link Node} recebido
     *
     * @param node Nó para adicionar na árvore
     */
    private void addNode(Node node) {
        final Node parent = node.getParent();
        final TreeNodeDTO dto = nodeMap.computeIfAbsent(node.getId(), id -> new TreeNodeDTO(node));
        if (parent == null) {
            root = dto;
        } else {
            nodeMap.computeIfAbsent(parent.getId(), id -> new TreeNodeDTO(parent)).getChildren().add(dto);
        }
    }
}
