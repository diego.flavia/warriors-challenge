WITH RECURSIVE MyTree AS (
    SELECT * FROM node WHERE parent_id IS NULL
    UNION ALL
    SELECT m.* FROM node AS m JOIN MyTree AS t ON m.parent_id = t.id
);

SELECT * FROM node;

select  id,
        code,
        parent_id 
from    (select * from node
         order by parent_id, id) node_sorted,
        (select @pv := 1) initialisation
where   find_in_set(parent_id, @pv)
and     length(@pv := concat(@pv, ',', id))