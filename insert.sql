SELECT * FROM node;

insert into node (code, description, detail, parent_id) 
values ('node_1', 'Node 0', 'Node of number 1', null);
insert into node (code, description, detail, parent_id) 
values ('node_1_1', 'Node 1.1', 'Node of number 1.1', 1);
insert into node (code, description, detail, parent_id) 
values ('node_1_2', 'Node 1.2', 'Node of number 1.2', 1);
insert into node (code, description, detail, parent_id) 
values ('node_1_3', 'Node 1.3', 'Node of number 1.3', 1);
insert into node (code, description, detail, parent_id) 
values ('node_1_1_1', 'Node 1.1.1', 'Node of number 1.1.1', 2);
insert into node (code, description, detail, parent_id) 
values ('node_1_1_2', 'Node 1.1.2', 'Node of number 1.1.2', 2);
insert into node (code, description, detail, parent_id) 
values ('node_1_1_3', 'Node 1.1.3', 'Node of number 1.1.3', 2);
insert into node (code, description, detail, parent_id) 
values ('node_1_2_1', 'Node 1.2.1', 'Node of number 1.1.1', 3);
insert into node (code, description, detail, parent_id) 
values ('node_1_2_2', 'Node 1.2.2', 'Node of number 1.2.2', 3);
insert into node (code, description, detail, parent_id) 
values ('node_1_2_3', 'Node 1.2.3', 'Node of number 1.2.3', 3);
insert into node (code, description, detail, parent_id) 
values ('node_1_3_1', 'Node 1.3.1', 'Node of number 1.3.1', 4);